#! user/bin/ruby

#====================================================================
# Author: Korshilovskiy (https://www.linkedin.com/in/korshilovskiy)
# Requirements:
# * ImageMagick 6.8.7+: http://www.imagemagick.org
# You can install it for Mac via HomeBrew: brew install imagemagick

def ProcessIcons()
    
    if (ARGV.length != 1)
        puts "Missing argument. first argument - icon file name to process"
        return
    end
    
    iosIcon = ARGV[0]
    
    if !File.exist?(iosIcon)
        puts "File '#{iosIcon}' doesn't exist!"
        return;
    end

    puts("Processing file #{iosIcon} ...")

    system("convert -resize 20x20 '#{iosIcon}' 'Icon-20.png'")
    system("convert -resize 40x40 '#{iosIcon}' 'Icon-20@2x.png'")
    system("convert -resize 60x60 '#{iosIcon}' 'Icon-20@3x.png'")

    system("convert -resize 29x29 '#{iosIcon}' 'Icon-29.png'")
    system("convert -resize 58x58 '#{iosIcon}' 'Icon-29@2x.png'")
    system("convert -resize 87x87 '#{iosIcon}' 'Icon-29@3x.png'")

    system("convert -resize 40x40 '#{iosIcon}' 'Icon-40.png'")
    system("convert -resize 80x80 '#{iosIcon}' 'Icon-40@2x.png'")
    system("convert -resize 120x120 '#{iosIcon}' 'Icon-40@3x.png'")

    system("convert -resize 120x120 '#{iosIcon}' 'Icon-60@2x.png'")
    system("convert -resize 180x180 '#{iosIcon}' 'Icon-60@3x.png'")

    system("convert -resize 76x76 '#{iosIcon}' 'Icon-76.png'")
    system("convert -resize 152x152 '#{iosIcon}' 'Icon-76@2x.png'")
    
    system("convert -resize 167x167 '#{iosIcon}' 'Icon-83-5@2x.png'")

    system("convert -resize 1024x1024 '#{iosIcon}' 'Icon-1024.png'")
    
    puts("Done.")
end

ProcessIcons()