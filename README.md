# Math Scrabble

MathScrabble is a board minigame about simple math calculations.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for overview and testing purposes.

### Prerequisites

* Cocos2d-x v3.16+, see cocos2d-x [requirements](https://github.com/cocos2d/cocos2d-x#build-requirements)
* Mac OS X 10.12+, Xcode 9+

### Installing

1. [Setup Cocos2d-x](https://github.com/cocos2d/cocos2d-x#git-user-attention)

2. Clone MathScrabble repository

			git clone git@bitbucket.org:Malaar/mathscrabble.git

3. Done.

## Running

To run on iOS/mac

Navigate to /proj.ios_mac/ and open MathScrabble.xcodeproj in Xcode

Or run a command in terminal:
			
			cocos run -p ios

Enjoy.

## Built With

* C++ - Language
* [Cocos2d-x](cocos2d-x.org) - Game engine

## Tutorial

* Match symbols and numbers in the equation
![Page 1](/ReadmeDocs/Tutorial1.png =320x)

* Get points when the result is 10, 20... or more
![Page 1](/ReadmeDocs/Tutorial2.png =320x)

* Connect consequtive numbers into the two-digit numbers
![Page 1](/ReadmeDocs/Tutorial3.png =320x)

* Use a special number to double your result
![Page 1](/ReadmeDocs/Tutorial4.png =320x)

## Authors

* **[Andrew Korshilovskiy](http://www.linkedin.com/in/korshilovskiy)** - development
* **[Oleksii Satsuta](https://www.facebook.com/aleksey.satsuta)** - gamedesign, design

## License

(c) All rights reserved.
This project is licensed under the **Proprietary Software License** - see the [LICENSE](http://www.binpress.com/license/view/l/358023be402acff778a934083b76b86f) url for details
