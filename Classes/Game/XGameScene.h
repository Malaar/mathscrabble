//
//  XGameScene.h
//  MathScrabble
//
//  Created by Malaar on 11/15/17.
//

#ifndef XGameScene_h
#define XGameScene_h

#include "cocos2d.h"
#include "XGameBoard.h"

class XGameScene : public cocos2d::Scene
{
public:
    CREATE_FUNC(XGameScene);
    XGameScene();
    virtual bool init();
    
    virtual void onEnter();
    virtual void onEnterTransitionDidFinish();

    bool onTouchBeganEvent(cocos2d::Touch* touch, cocos2d::Event* event);
    bool onTouchEndedEvent(cocos2d::Touch* touch, cocos2d::Event* event);
    bool onTouchMovedEvent(cocos2d::Touch* touch, cocos2d::Event* event);
    
protected:
    void loadAssets();
    void configureGUI();
    
    void startGame();
    void pauseGame();
    void resumeGame();
    void gameOver();
    void leaveGame(bool animateTransition);
    void freeze();
    void unfreeze();

    void setTotalScore(int score);
    void showReceivedScore(int receivedScore);

private:
    cocos2d::ProgressTimer* timeProgressNode;
    cocos2d::Label* scoreLabel;
    cocos2d::Label* receivedScoreLabel;
    XGameBoard* gameBoard;

    bool gameOverFlag;
    bool gameFreeze;
    int totalScore;
//    int plaingTime;
//    int currentPlaingTime;
//    int freezeTime;
//    int currentFreezeTime;
//    bool playTimeSound;
};

#endif /* XGameScene_h */
