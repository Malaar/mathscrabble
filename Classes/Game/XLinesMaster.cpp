/* 
 * File:   XLinesMaster.cpp
 * Author: malaar
 * 
 * Created on 27 Март 2015 г., 18:10
 */

#include "XLinesMaster.h"
#include "XFilePath.h"
#include "XPolarCoord2D.h"
#include "XConvertor.h"

static const XID kXInvisibleState   = XID::GetByName("invisible");
static const XID kXVisibleState     = XID::GetByName("visible");
static const XID kXShowState        = XID::GetByName("show");
static const XID kXHideState        = XID::GetByName("hide");

XLinesMaster::XLinesMaster()
{
    owner = NULL;
    
    XString path = XFilePath::PathForResourceInBundle("Scenes/Game/Line.xml");
    XSprite* templateSprite = (XSprite*)XNode::LoadFromFile(path);
    if(templateSprite)
    {
        pool.Fill(templateSprite, 24);
    }
}
    
void XLinesMaster::SetLinesOwner(XSprite* owner)
{
    this->owner = owner;
}

xbool XLinesMaster::LinkTails(XTile* tile1, XTile* tile2)
{
    if(!tile1 || !tile2)
        return false;

    xbool result = false;
    XVector2D position = tile2->GetPosition() - tile1->GetPosition();
    XPolarCoord2D polar = XConvertor::VectorToPolar(position);
    polar.ro /= 2.0f;
    position = tile1->GetPosition() + XConvertor::PolarToVector(polar);
    position.y -= 6;
    XSprite* line = pool.GetFreeSprite();
    if(line)
    {
        line->SetState(kXInvisibleState);
        line->SetPosition(position);
        line->SetRotation(polar.alpha);
        lines.push_back(line);
        line->SetState(kXShowState);
        if(owner)
            owner->AttachNode(line);
        
        result = true;
    };
    return result;
}

void XLinesMaster::UnlinkAllTiles()
{
    XSpriteVectorIt it = lines.begin();
    XSpriteVectorIt end = lines.end();
    for(; it != end; ++it)
    {
        pool.ReleaseSprite(*it);
        (*it)->DetachFromParent();
//        (*it)->SetState(kXHideState);
    }
    lines.clear();
}
