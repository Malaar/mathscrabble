/* 
 * File:   XTileOperator.h
 * Author: malaar
 *
 * Created on 23 Март 2015 г., 15:03
 */

#ifndef XTILEOPERATOR_H
#define	XTILEOPERATOR_H

#include "XTile.h"
#include "XExpressionOperator.h"

class XTileOperator : public XTile
{
public:
    enum XTileOperatorType
    {
        XTileOperatorTypeAdd,
        XTileOperatorTypeSub,
        XTileOperatorTypeMult,
        XTileOperatorTypeDiv
    };
    
    CREATE_FUNC(XTileOperator);
    XTileOperator();

    virtual bool init();

    virtual void select();
    virtual void deselect();
    virtual void indicateAsWrong();

    virtual XExpressionOperator* generateExpressionItem() const;

    void setOperatorType(XTileOperatorType type);
    inline XTileOperatorType getOperatorType() const;
    
private:
    XTileOperatorType operatorType;
};

inline XTileOperator::XTileOperatorType XTileOperator::getOperatorType() const
{
    return operatorType;
}

#endif	/* XTILEOPERATOR_H */

