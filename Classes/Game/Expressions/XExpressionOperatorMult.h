/* 
 * File:   XExpressionOperatorMult.h
 * Author: malaar
 *
 * Created on 24 Март 2015 г., 23:43
 */

#ifndef XEXPRESSIONOPERATORMULT_H
#define	XEXPRESSIONOPERATORMULT_H

#include "XExpressionOperator.h"

class XExpressionOperatorMult : public XExpressionOperator
{
public:
    XExpressionOperatorMult();
    virtual XExpressionOperatorMult* clone() const;
    virtual bool isEqual(const XExpressionItem* item);
    virtual XExpressionNumber evaluate() const;
};

#endif	/* XEXPRESSIONOPERATORMULT_H */

