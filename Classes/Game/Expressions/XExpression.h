/* 
 * File:   XExpression.h
 * Author: malaar
 *
 * Created on 24 Март 2015 г., 14:00
 */

#ifndef XEXPRESSION_H
#define	XEXPRESSION_H

#include "XExpressionNumber.h"
#include "XExpressionOperatorAdd.h"
#include "XExpressionOperatorSub.h"
#include "XExpressionOperatorMult.h"
#include "XExpressionOperatorDiv.h"
#include "XExpressionOperatorGlide.h"

class XExpression : public cocos2d::Clonable
{
public:
    XExpression();
    XExpression(const XExpression& expression);
    virtual ~XExpression();
    virtual XExpression* clone() const;

    inline void setMaxOperatorsCount(xuint max);
    inline void setMaxGlidesCount(xuint max);
    inline xuint getMaxOperatorsCount() const;
    inline xuint getMaxGlidesCount() const;
    
    void setItems(const XExpressionItemVector& items);
    bool pushItem(XExpressionItem* item);
    void clearItems();
    bool hasItem(const XExpressionItem* item) const;

    XExpressionNumber evaluate() const;
    
    bool isCommutative() const;
    bool isEqual(const XExpression* expression) const;
    
private:
    bool canAddItem(XExpressionItem* item);
    XExpressionItem* itemByID(const std::string& ID) const;
    const XExpressionItem* getLastItem() const;
    xuint getOperatorsCount() const;
    xuint getGlideOperatorsCount() const;
    bool canEvaluate() const;
    XExpressionNumber evaluateInner() const;
    bool getHighesOperatorInfo(xuint* operatorIndex, XExpressionOperator** highestOperator) const;
    XExpression* upgradeToExpression(xuint operatorIndex, XExpressionOperator* highestOperator) const;
    
    XExpressionItemVector items;
    xuint maxOperatorsCount;
    xuint maxGlidesCount;
};

typedef std::vector<XExpression*> XExpressionVector;
typedef XExpressionVector::iterator XExpressionVectorIt;
typedef XExpressionVector::const_iterator XExpressionVectorConstIt;

inline void XExpression::setMaxOperatorsCount(xuint max)
{
    maxOperatorsCount = max;
}

inline void XExpression::setMaxGlidesCount(xuint max)
{
    maxGlidesCount = max;
}

inline xuint XExpression::getMaxOperatorsCount() const
{
    return maxOperatorsCount;
}

inline xuint XExpression::getMaxGlidesCount() const
{
    return maxGlidesCount;
}

#endif	/* XEXPRESSION_H */

