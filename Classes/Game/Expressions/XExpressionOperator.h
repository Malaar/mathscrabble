/* 
 * File:   XExpressionOperator.h
 * Author: malaar
 *
 * Created on 24 Март 2015 г., 23:22
 */

#ifndef XEXPRESSIONOPERATOR_H
#define	XEXPRESSIONOPERATOR_H

#include "XExpressionNumber.h"

class XExpressionOperator : public XExpressionItem
{
public:
    inline static bool isOperator(const XExpressionItem* item);
    
    XExpressionOperator();
    XExpressionOperator(const XExpressionOperator& op);
    ~XExpressionOperator() = 0;
    
    virtual bool isEqual(const XExpressionItem* item) = 0;
    inline bool isCommutative() const;

    virtual XExpressionNumber evaluate() const;

    inline xuint8 getPriority() const;
    void setOperands(XExpressionNumber* left, XExpressionNumber* right);
    
protected:
    xuint8 priority;
    bool commutative;
    XExpressionNumber* leftOperand;
    XExpressionNumber* rightOperand;
};

typedef std::vector<XExpressionOperator*> XExpressionOperatorVector;
typedef XExpressionOperatorVector::iterator XExpressionOperatorVectorIt;
typedef XExpressionOperatorVector::const_iterator XExpressionOperatorVectorConstIt;

inline xuint8 XExpressionOperator::getPriority() const
{
    return priority;
}

inline bool XExpressionOperator::isCommutative() const
{
    return commutative;
}

inline bool XExpressionOperator::isOperator(const XExpressionItem* item)
{
    return item && dynamic_cast<const XExpressionOperator*>(item);
}

#endif	/* XEXPRESSIONOPERATOR_H */
