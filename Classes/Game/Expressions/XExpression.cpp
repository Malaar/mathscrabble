/* 
 * File:   XExpression.cpp
 * Author: malaar
 * 
 * Created on 24 Март 2015 г., 14:00
 */

#include "XExpression.h"
#include "XExpressionOperator.h"
#include "XExpressionNumber.h"

XExpression::XExpression()
{
    maxOperatorsCount = 0;
    maxGlidesCount = 0;
}

XExpression::XExpression(const XExpression& expression)
{
    maxOperatorsCount = expression.maxOperatorsCount;
    maxGlidesCount = expression.maxGlidesCount;
    setItems(expression.items);
}

XExpression::~XExpression()
{
    clearItems();
}

XExpression* XExpression::clone() const
{
    return new XExpression(*this);
}

void XExpression::setItems(const XExpressionItemVector& items)
{
    clearItems();
    XExpressionItemVectorConstIt it = items.begin();
    XExpressionItemVectorConstIt end = items.end();
    for(; it != end; ++it)
    {
        this->items.push_back(static_cast<XExpressionItem*>((*it)->clone()));
    }
}

const XExpressionItem* XExpression::getLastItem() const
{
    return items.size() > 0 ? items.back() : (const XExpressionItem*)nullptr;
}

bool XExpression::pushItem(XExpressionItem* item)
{
    bool result = false;
    if(canAddItem(item))
    {
        result = true;
        items.push_back(item);
    }
    return result;
}

void XExpression::clearItems()
{
    XExpressionItemVectorIt it = items.begin();
    XExpressionItemVectorIt end = items.end();
    for(; it != end; ++it)
    {
        CC_SAFE_DELETE(*it);
    }
    items.clear();
}

XExpressionNumber XExpression::evaluate() const
{
    XExpressionNumber result(0);
    
    if(canEvaluate())
    {
        result = evaluateInner();
    }
    
    return result;
}

bool XExpression::canEvaluate() const
{
    bool result = false;
    
    if(items.size() >= 3)
    {
        xuint opCount = getOperatorsCount();
        if(opCount > 0 && opCount <= maxOperatorsCount)
            result = true;
    }
    
    return result;
}

XExpressionNumber XExpression::evaluateInner() const
{
    XExpressionNumber result(0);
    
    if(items.size() == 3)
    {
        XExpressionNumber* number1 = (XExpressionNumber*)items[0];
        XExpressionNumber* number2 = (XExpressionNumber*)items[2];
        ((XExpressionOperator*)items[1])->setOperands(number1, number2);
        result = ((XExpressionOperator*)items[1])->evaluate();
    }
    else if(items.size() > 3)
    {
        // find operator with highest priority
        xuint index = 1;
        XExpressionOperator* op = (XExpressionOperator*)items[index];        
        if(getHighesOperatorInfo(&index, &op))
        {
            // upgrade expression, to exclude this operator (1 step of calculation))
            XExpression* expression = upgradeToExpression(index, op);
            if(expression)
            {
                result = expression->evaluate();
                CC_SAFE_DELETE(expression);
            }
        }
    }
    
    return result;
}

bool XExpression::canAddItem(XExpressionItem* item)
{
    if(!item)
        return false;

    bool result = false;
    const XExpressionItem* lastItem = getLastItem();
    if(lastItem)
    {
        // expression doesn't contain item with same ID
        // AND
        // operators count < 3 (except glide operator)
        xuint opCount = getOperatorsCount() + (XExpressionOperator::isOperator(item) ? 1 : 0);
        if(opCount <= maxOperatorsCount && !itemByID(item->getID()))
        {
            // last item - Number
            if(XExpressionNumber::isNumber(lastItem))
            {
                if(XExpressionNumber::isNumber(item))
                {
                    // only in case of Glide Number can follow after Number
                    if(XExpressionOperatorGlide::canApplayGlide(lastItem, item))
                    {
                        // glide operators is limited per expression
                        if(getGlideOperatorsCount() + 1 <= maxGlidesCount)
                        {
                            items.push_back(new XExpressionOperatorGlide());
                            result = true;
                        }
                    }
                }
                else
                {
                    result = true;
                }
//                XExpressionOperator::isOperator(item)
            }
            // last item - Operator
            else if(XExpressionNumber::isNumber(item))  // after operator can add only Number
            {
                result = true;
            }
        }
    }
    // no items
    else if(XExpressionNumber::isNumber(item))  // first new item can only be Number
    {
        result = true;
    }
    
    return result;
}

xuint XExpression::getOperatorsCount() const
{
    xuint result = 0;
    XExpressionItemVectorConstIt it = items.begin();
    XExpressionItemVectorConstIt end = items.end();
    for(; it != end; ++it)
    {
        if(XExpressionOperator::isOperator(*it))
        {
            if(!dynamic_cast<const XExpressionOperatorGlide*>(*it))
            {
                result++;
            }
        }
    }
    return result;
}

xuint XExpression::getGlideOperatorsCount() const
{
    xuint result = 0;
    XExpressionItemVectorConstIt it = items.begin();
    XExpressionItemVectorConstIt end = items.end();
    for(; it != end; ++it)
    {
        if(XExpressionOperator::isOperator(*it))
        {
            if(dynamic_cast<const XExpressionOperatorGlide*>(*it))
            {
                result++;
            }
        }
    }
    return result;
}

XExpressionItem* XExpression::itemByID(const std::string& ID) const
{
    XExpressionItem* result = nullptr;
    XExpressionItemVectorConstIt it = items.begin();
    XExpressionItemVectorConstIt end = items.end();
    for(; it != end; ++it)
    {
        if((*it)->getID() == ID)
        {
            result = *it;
            break;
        }
    }
    return result;
}

bool XExpression::getHighesOperatorInfo(xuint* operatorIndex, XExpressionOperator** highestOperator) const
{
    bool result = false;
    if(items.size() >= 3 && operatorIndex && highestOperator)
    {
        // assume, that operators on indexes like 1,3,5, etc
        xuint index = 1;
        XExpressionOperator* op = (XExpressionOperator*)items[index];
        XExpressionOperator* testedOp = nullptr;
        for(xuint i = 3; i < items.size(); i += 2)
        {
            testedOp = (XExpressionOperator*)items[i];
            if(testedOp->getPriority() > op->getPriority())
            {
                index = i;
                op = testedOp;
            }
        }
        *operatorIndex = index;
        *highestOperator = op;
        result = true;
    }
    return result;
}

XExpression* XExpression::upgradeToExpression(xuint operatorIndex, XExpressionOperator* highestOperator) const
{
    XExpression* result = nullptr;
    if(items.size() >= 3 && operatorIndex < items.size() - 1 && highestOperator)
    {
        XExpressionNumber* number1 = (XExpressionNumber*)items[operatorIndex - 1];
        XExpressionNumber* number2 = (XExpressionNumber*)items[operatorIndex + 1];
        highestOperator->setOperands(number1, number2);
        XExpressionNumber* number = highestOperator->evaluate().clone();

        XExpressionItemVector newItems = items;

        // erase next items: i-1, i, i + 1
        operatorIndex = operatorIndex - 1;
        newItems.erase(newItems.begin() + operatorIndex);
        newItems.erase(newItems.begin() + operatorIndex);
        newItems.erase(newItems.begin() + operatorIndex);
        newItems.insert(newItems.begin() + operatorIndex, number);

        result = new XExpression();
        result->setMaxOperatorsCount(getMaxOperatorsCount());
        result->setMaxGlidesCount(getMaxGlidesCount());
        result->setItems(newItems);

        CC_SAFE_DELETE(number);
    }
    return result;
}

bool XExpression::hasItem(const XExpressionItem* item) const
{
    bool result = false;
    XExpressionItemVectorConstIt it = items.begin();
    XExpressionItemVectorConstIt end = items.end();
    for(; it != end; ++it)
    {
        if((*it)->isEqual(item))
        {
            result = true;
            break;
        }
    }
    return result;
}

bool XExpression::isCommutative() const
{
    bool result = false;

    bool operatorsEqual = true;
    XExpressionOperator* testOp = nullptr;
    XExpressionItemVectorConstIt it = items.begin();
    XExpressionItemVectorConstIt end = items.end();
    for(; it != end; ++it)
    {
        if(XExpressionOperator::isOperator(*it))
        {
            if(!testOp)
                testOp = (XExpressionOperator*)(*it);
            else if(!testOp->isEqual(*it))
            {
                operatorsEqual = false;
                break;
            }
        }
    }
    if(operatorsEqual && testOp)
    {
        result = testOp->isCommutative();
    }

    return result;
}

bool XExpression::isEqual(const XExpression* expression) const
{
    bool result = false;
    
    if(expression && items.size() == expression->items.size())
    {
        if(items.size() == 3)
        {
            if(items[1]->isEqual(expression->items[1])) // is equal operators
            {
                if(((XExpressionOperator*)items[1])->isCommutative())
                {
                    result = (items[0]->isEqual(expression->items[0]) && 
                             items[2]->isEqual(expression->items[2]))
                             ||
                             (items[0]->isEqual(expression->items[2]) && 
                             items[2]->isEqual(expression->items[0]));
                }
                else
                {
                    // is equal numbers
                    result = items[0]->isEqual(expression->items[0]) && 
                             items[2]->isEqual(expression->items[2]);
                }
            }
        }
        else if(items.size() > 3)
        {
            XExpressionOperator* op1 = (XExpressionOperator*)items[1];
            XExpressionOperator* op2 = (XExpressionOperator*)expression->items[1];
            // both expression full commutative and have the same operators (ex: n1*n2*n3 == m1*m2*m3, n1+n2+n3 == m1+m2+m3)
            if(op1->isEqual(op2) && op1->isCommutative() && isCommutative() && expression->isCommutative())
            {
                // check numbers equality
                bool equalsNumbers = true;
                for(int i = 0; i < items.size(); i += 2)
                {
                    if(!expression->hasItem(items[i]))
                    {
                        equalsNumbers = false;
                        break;
                    }
                }
                
                if(equalsNumbers)
                {
                    result = true;
                }
            }
            else
            {
                xuint index1, index2;
                op1 = nullptr, op2 = nullptr;
                if(getHighesOperatorInfo(&index1, &op1) && expression->getHighesOperatorInfo(&index2, &op2))
                {
                    if(op1 && op1->isEqual(op2))
                    {
                        XExpression* exp1 = upgradeToExpression(index1, op1);
                        XExpression* exp2 = expression->upgradeToExpression(index2, op2);
                        result = exp1 && exp1->isEqual(exp2);
                    }
                }
            }
        }
    }
    return result;
}
