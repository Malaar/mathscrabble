/* 
 * File:   XExpressionItem.h
 * Author: malaar
 *
 * Created on 24 Март 2015 г., 23:20
 */

#ifndef XEXPRESSIONITEM_H
#define	XEXPRESSIONITEM_H

#include "cocos2d.h"

typedef unsigned int  xuint;
typedef unsigned char xuint8;

class XExpressionItem : public cocos2d::Clonable
{
public:
    inline void setID(const std::string& ID);
    inline const std::string& getID() const;
    virtual XExpressionItem* clone() const = 0;
    virtual bool isEqual(const XExpressionItem* item) = 0;

protected:
    std::string ID;
};

typedef std::vector<XExpressionItem*> XExpressionItemVector;
typedef XExpressionItemVector::iterator XExpressionItemVectorIt;
typedef XExpressionItemVector::const_iterator XExpressionItemVectorConstIt;

inline void XExpressionItem::setID(const std::string& ID)
{
    this->ID = ID;
}

inline const std::string& XExpressionItem::getID() const
{
    return ID;
}

#endif	/* XEXPRESSIONITEM_H */

