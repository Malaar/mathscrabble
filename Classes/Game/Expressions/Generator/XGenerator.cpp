/* 
 * File:   XGenerator.cpp
 * Author: malaar
 * 
 * Created on 23 Март 2015 г., 16:29
 */

#include "XGenerator.h"
//#include "XBaseDefines.h"

XGenerator::XGenerator()
{
    XExpressionNumber* number;
    for(int i = 1; i < 10; ++i)
    {
        number = new XExpressionNumber(i);
        numbers.push_back(number);
    }
}

XGenerator::~XGenerator()
{
    ClearExpressions();
}

bool XGenerator::isExpressionAlreadyUsed(const XExpressionVector& expressions, const XExpression* expression) const
{
    bool result = false;
    if(expression)
    {
        XExpressionVectorConstIt it = expressions.begin();
        XExpressionVectorConstIt end = expressions.end();
        for(; it != end; ++it)
        {
            if((*it)->isEqual(expression))
            {
                result = true;
                break;
            }
        }
    }
    return result;
}

void XGenerator::TryItems(const XExpressionItemVector& items)
{
    XExpression* expression = new XExpression();
//    expression->setItems(items);

    bool success = false;
    XExpressionItemVectorConstIt it = items.begin();
    XExpressionItemVectorConstIt end = items.end();
    for(; it != end; ++it)
    {
        success = expression->pushItem(*it);
        if(!success)
            break;
    }

    if(success && expression->evaluate().getNumber() == 10 && !isExpressionAlreadyUsed(expressions, expression))
    {
        expressions.push_back(expression);
    }
    else
    {
        CC_SAFE_DELETE(expression);
    }
}

XExpressionOperator* XGenerator::OperatorFromType(int type)
{
    XExpressionOperator* result = nullptr;
    switch(type)
    {
        case -1:
            result = new XExpressionOperatorGlide();
            break;
        case -2:
            result = new XExpressionOperatorAdd();
            break;
        case -3:
            result = new XExpressionOperatorSub();
            break;
        case -4:
            result = new XExpressionOperatorMult();
            break;
        default:
            result = new XExpressionOperatorAdd();
    }
    return result;
}

void XGenerator::ClearExpressions()
{
    XExpressionVectorIt it = expressions.begin();
    XExpressionVectorIt end = expressions.end();
    for(; it != end; ++it)
    {
        CC_SAFE_DELETE(*it);
    }
    expressions.clear();
}

void XGenerator::Generate()
{
    ClearExpressions();
    
    // a <operator> b
    for(int t = -2; t > -5; --t)
    {
        for(int i = 1; i < 10; ++i)
        for(int j = 1; j < 10; ++j)
        {
            XExpressionItemVector items;
            items.push_back(new XExpressionNumber(i));
            items.push_back(OperatorFromType(t));
            items.push_back(new XExpressionNumber(j));
            TryItems(items);
        }
    }

    // a <operator1> b <operator2> c
    for(int t1 = -2; t1 > -5; --t1)
    for(int t2 = -2; t2 > -5; --t2)
    {
        for(int i = 1; i < 10; ++i)
        for(int j = 1; j < 10; ++j)
        for(int k = 1; k < 10; ++k)
        {
            XExpressionItemVector items;
            items.push_back(new XExpressionNumber(i));
            items.push_back(OperatorFromType(t1));
            items.push_back(new XExpressionNumber(j));
            items.push_back(OperatorFromType(t2));
            items.push_back(new XExpressionNumber(k));
            TryItems(items);
        }
    }

    // a <operator1> b <operator2> c <operator3> d
    for(int t1 = -1; t1 > -5; --t1)
    for(int t2 = -1; t2 > -5; --t2)
    for(int t3 = -1; t3 > -5; --t3)
    {
        for(int i = 1; i < 10; ++i)
        for(int j = 1; j < 10; ++j)
        for(int k = 1; k < 10; ++k)
        for(int l = 1; l < 10; ++l)
        {
            XExpressionItemVector items;
            items.push_back(new XExpressionNumber(i));
            items.push_back(OperatorFromType(t1));
            items.push_back(new XExpressionNumber(j));
            items.push_back(OperatorFromType(t2));
            items.push_back(new XExpressionNumber(k));
            items.push_back(OperatorFromType(t3));
            items.push_back(new XExpressionNumber(l));
            TryItems(items);
        }
    }
}
