/* 
 * File:   XGenerator.h
 * Author: malaar
 *
 * Created on 23 Март 2015 г., 16:29
 */

#ifndef XGenerator_H
#define	XGenerator_H

#include "XExpression.h"

class XGenerator
{
public:
    XGenerator();
    virtual ~XGenerator();
    
    void Generate();
    inline XExpressionVector GetExpressions() const;
    
private:
    bool isExpressionAlreadyUsed(const XExpressionVector& expressions, const XExpression* expression) const;
    void TryItems(const XExpressionItemVector& items);
    XExpressionOperator* OperatorFromType(int type);
    void ClearExpressions();

    XExpressionItemVector numbers;
    XExpressionVector expressions;
};

inline XExpressionVector XGenerator::GetExpressions() const
{
    return expressions;
}


#endif	/* XGAMEBOARD_H */
