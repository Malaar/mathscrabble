/* 
 * File:   XExpressionOperatorSub.cpp
 * Author: malaar
 * 
 * Created on 24 Март 2015 г., 23:43
 */

#include "XExpressionOperatorSub.h"

XExpressionOperatorSub::XExpressionOperatorSub()
{
    priority = 100;
}

XExpressionOperatorSub* XExpressionOperatorSub::clone() const
{
    return new XExpressionOperatorSub(*this);
}

bool XExpressionOperatorSub::isEqual(const XExpressionItem* item)
{
    return item && dynamic_cast<const XExpressionOperatorSub*>(item);
}

XExpressionNumber XExpressionOperatorSub::evaluate() const
{
    XExpressionNumber result = XExpressionOperator::evaluate();
    if(leftOperand && rightOperand)
    {
        result.setNumber(leftOperand->getNumber() - rightOperand->getNumber());
    }
    return result;
}
