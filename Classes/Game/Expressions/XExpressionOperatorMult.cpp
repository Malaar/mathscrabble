/* 
 * File:   XExpressionOperatorMult.cpp
 * Author: malaar
 * 
 * Created on 24 Март 2015 г., 23:43
 */

#include "XExpressionOperatorMult.h"

XExpressionOperatorMult::XExpressionOperatorMult()
{
    priority = 128;
    commutative = true;
}

XExpressionOperatorMult* XExpressionOperatorMult::clone() const
{
    return new XExpressionOperatorMult(*this);
}

bool XExpressionOperatorMult::isEqual(const XExpressionItem* item)
{
    return item && dynamic_cast<const XExpressionOperatorMult*>(item);
}

XExpressionNumber XExpressionOperatorMult::evaluate() const
{
    XExpressionNumber result = XExpressionOperator::evaluate();
    if(leftOperand && rightOperand)
    {
        result.setNumber(leftOperand->getNumber() * rightOperand->getNumber());
    }
    return result;
}
