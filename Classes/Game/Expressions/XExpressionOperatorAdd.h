/* 
 * File:   XExpressionOperatorAdd.h
 * Author: malaar
 *
 * Created on 24 Март 2015 г., 23:42
 */

#ifndef XEXPRESSIONOPERATORADD_H
#define	XEXPRESSIONOPERATORADD_H

#include "XExpressionOperator.h"

class XExpressionOperatorAdd : public XExpressionOperator
{
public:
    XExpressionOperatorAdd();
    virtual XExpressionOperatorAdd* clone() const;
    virtual bool isEqual(const XExpressionItem* item);
    virtual XExpressionNumber evaluate() const;
};

#endif	/* XEXPRESSIONOPERATORADD_H */

