/* 
 * File:   XExpressionNumber.cpp
 * Author: malaar
 * 
 * Created on 24 Март 2015 г., 23:22
 */

#include "XExpressionNumber.h"

XExpressionNumber::XExpressionNumber(int number)
{
    setNumber(number);
    setSpecial(false);
}

XExpressionNumber* XExpressionNumber::clone() const
{
    return new XExpressionNumber(*this);
}

bool XExpressionNumber::isEqual(const XExpressionItem* item)
{
    bool result = false;
    if(XExpressionNumber::isNumber(item))
    {
        result = getNumber() == ((XExpressionNumber*)item)->getNumber();
    }
    return result;
}

void XExpressionNumber::setNumber(int number)
{
    this->number = number;
}
