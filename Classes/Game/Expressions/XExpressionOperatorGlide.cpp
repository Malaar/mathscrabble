/* 
 * File:   XExpressionOperatorGlide.cpp
 * Author: malaar
 * 
 * Created on 24 Март 2015 г., 23:44
 */

#include "XExpressionOperatorGlide.h"

XExpressionOperatorGlide::XExpressionOperatorGlide()
{
    priority = 255;
}

XExpressionOperatorGlide* XExpressionOperatorGlide::clone() const
{
    return new XExpressionOperatorGlide(*this);
}

bool XExpressionOperatorGlide::isEqual(const XExpressionItem* item)
{
    return item && dynamic_cast<const XExpressionOperatorGlide*>(item);
}

XExpressionNumber XExpressionOperatorGlide::evaluate() const
{
    XExpressionNumber result = XExpressionOperator::evaluate();
    if(leftOperand && rightOperand)
    {
        result.setNumber(10 * leftOperand->getNumber() + rightOperand->getNumber());
        // mark result as glided!
    }
    return result;
}

bool XExpressionOperatorGlide::canApplayGlide(const XExpressionItem* left, const XExpressionItem* right)
{
    bool result = false;
    if(left && right)
    {
        if(XExpressionNumber::isNumber(left) && XExpressionNumber::isNumber(right))
        {
            result = ((const XExpressionNumber*)right)->getNumber() - ((const XExpressionNumber*)left)->getNumber() == 1;
        }
    }
    
    return result;
}
