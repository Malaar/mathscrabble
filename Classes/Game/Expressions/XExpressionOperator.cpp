/* 
 * File:   XExpressionOperator.cpp
 * Author: malaar
 * 
 * Created on 24 Март 2015 г., 23:22
 */

#include "XExpressionOperator.h"
#include "XExpressionOperatorAdd.h"

XExpressionOperator::XExpressionOperator()
{
    priority = 0;
    commutative = false;
    leftOperand = nullptr;
    rightOperand = nullptr;
}

XExpressionOperator::XExpressionOperator(const XExpressionOperator& op)
                   : XExpressionItem(op)
{
    priority = op.priority;
    commutative = op.commutative;
    leftOperand = nullptr;
    rightOperand = nullptr;
    setOperands(op.leftOperand, op.rightOperand);
}

XExpressionOperator::~XExpressionOperator()
{
    setOperands(nullptr, nullptr);
}

void XExpressionOperator::setOperands(XExpressionNumber* left, XExpressionNumber* right)
{
    CC_SAFE_DELETE(leftOperand);
    CC_SAFE_DELETE(rightOperand);

    leftOperand = left ? left->clone() : nullptr;
    rightOperand = right ? right->clone() : nullptr;
}

XExpressionNumber XExpressionOperator::evaluate() const
{
    XExpressionNumber result(0);
    if(leftOperand && rightOperand)
    {
        result.setSpecial(leftOperand->isSpecial() || rightOperand->isSpecial());
    }
    return result;
}
