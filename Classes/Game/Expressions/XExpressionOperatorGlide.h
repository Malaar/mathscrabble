/* 
 * File:   XExpressionOperatorGlide.h
 * Author: malaar
 *
 * Created on 24 Март 2015 г., 23:44
 */

#ifndef XEXPRESSIONOPERATORGLIDE_H
#define	XEXPRESSIONOPERATORGLIDE_H

#include "XExpressionOperator.h"

class XExpressionOperatorGlide : public XExpressionOperator
{
public:
    XExpressionOperatorGlide();
    virtual XExpressionOperatorGlide* clone() const;
    virtual bool isEqual(const XExpressionItem* item);
    virtual XExpressionNumber evaluate() const;
    
    static bool canApplayGlide(const XExpressionItem* left, const XExpressionItem* right);
};

#endif	/* XEXPRESSIONOPERATORGLIDE_H */

