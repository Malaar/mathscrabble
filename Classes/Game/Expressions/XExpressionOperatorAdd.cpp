/* 
 * File:   XExpressionOperatorAdd.cpp
 * Author: malaar
 * 
 * Created on 24 Март 2015 г., 23:42
 */

#include "XExpressionOperatorAdd.h"

XExpressionOperatorAdd::XExpressionOperatorAdd()
{
    priority = 100;
    commutative = true;
}

XExpressionOperatorAdd* XExpressionOperatorAdd::clone() const
{
    return new XExpressionOperatorAdd(*this);
}

bool XExpressionOperatorAdd::isEqual(const XExpressionItem* item)
{
    return item && dynamic_cast<const XExpressionOperatorAdd*>(item);
}

XExpressionNumber XExpressionOperatorAdd::evaluate() const
{
    XExpressionNumber result = XExpressionOperator::evaluate();
    if(leftOperand && rightOperand)
    {
        result.setNumber(leftOperand->getNumber() + rightOperand->getNumber());
    }
    return result;
}
