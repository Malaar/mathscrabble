/* 
 * File:   XExpressionNumber.h
 * Author: malaar
 *
 * Created on 24 Март 2015 г., 23:22
 */

#ifndef XEXPRESSIONNUMBER_H
#define	XEXPRESSIONNUMBER_H

#include "XExpressionItem.h"

class XExpressionNumber : public XExpressionItem
{
public:
    
    inline static bool isNumber(const XExpressionItem* item);
    
    XExpressionNumber(int number = 0);
    virtual XExpressionNumber* clone() const;

    virtual bool isEqual(const XExpressionItem* item);
        
    void setNumber(int number);
    inline int getNumber() const;
    
    inline void setSpecial(bool special);
    inline bool isSpecial() const;
    
private:
    int number;
    bool special;
};

inline int XExpressionNumber::getNumber() const
{
    return number;
}

inline void XExpressionNumber::setSpecial(bool special)
{
    this->special = special;
}

inline bool XExpressionNumber::isSpecial() const
{
    return special;
}

inline bool XExpressionNumber::isNumber(const XExpressionItem* item)
{
    return item && dynamic_cast<const XExpressionNumber*>(item);
}

#endif	/* XEXPRESSIONNUMBER_H */

