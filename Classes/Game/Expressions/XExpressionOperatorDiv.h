/* 
 * File:   XExpressionOperatorDiv.h
 * Author: malaar
 *
 * Created on 24 Март 2015 г., 23:43
 */

#ifndef XEXPRESSIONOPERATORDIV_H
#define	XEXPRESSIONOPERATORDIV_H

#include "XExpressionOperator.h"

class XExpressionOperatorDiv : public XExpressionOperator
{
public:
    XExpressionOperatorDiv();
    virtual XExpressionOperatorDiv* clone() const;
    virtual bool isEqual(const XExpressionItem* item);
    virtual XExpressionNumber evaluate() const;
};

#endif	/* XEXPRESSIONOPERATORDIV_H */

