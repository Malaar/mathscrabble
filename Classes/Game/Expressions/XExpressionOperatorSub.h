/* 
 * File:   XExpressionOperatorSub.h
 * Author: malaar
 *
 * Created on 24 Март 2015 г., 23:43
 */

#ifndef XEXPRESSIONOPERATORSUB_H
#define	XEXPRESSIONOPERATORSUB_H

#include "XExpressionOperator.h"

class XExpressionOperatorSub : public XExpressionOperator
{
public:
    XExpressionOperatorSub();
    virtual XExpressionOperatorSub* clone() const;
    virtual bool isEqual(const XExpressionItem* item);
    virtual XExpressionNumber evaluate() const;
};

#endif	/* XEXPRESSIONOPERATORSUB_H */

