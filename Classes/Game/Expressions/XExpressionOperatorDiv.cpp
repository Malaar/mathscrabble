/* 
 * File:   XExpressionOperatorDiv.cpp
 * Author: malaar
 * 
 * Created on 24 Март 2015 г., 23:43
 */

#include "XExpressionOperatorDiv.h"

XExpressionOperatorDiv::XExpressionOperatorDiv()
{
    priority = 128;
}

XExpressionOperatorDiv* XExpressionOperatorDiv::clone() const
{
    return new XExpressionOperatorDiv(*this);
}

bool XExpressionOperatorDiv::isEqual(const XExpressionItem* item)
{
    return item && dynamic_cast<const XExpressionOperatorDiv*>(item);
}

XExpressionNumber XExpressionOperatorDiv::evaluate() const
{
    XExpressionNumber result = XExpressionOperator::evaluate();
    if(leftOperand && rightOperand && rightOperand->getNumber() != 0)
    {
        result.setNumber(leftOperand->getNumber() / rightOperand->getNumber());
    }
    return result;
}
