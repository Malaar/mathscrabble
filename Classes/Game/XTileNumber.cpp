/* 
 * File:   XTileNumber.cpp
 * Author: malaar
 * 
 * Created on 23 Март 2015 г., 15:03
 */

#include "XTileNumber.h"

USING_NS_CC;

#define Super XTile

XTileNumber::XTileNumber()
{
    number = 0;
    special = false;
}

bool XTileNumber::init()
{
    if(!Super::init())
    {
        return false;
    }
    
    //
    if(tile)
    {
        title = Label::createWithBMFont("fonts/Cooper30.fnt", "0");
        tile->addChild(title);
        title->setLocalZOrder(10);
        title->setPositionNormalized(Vec2(0.5, 0.43));
    }

    return true;
}

void XTileNumber::select()
{
    if(tileState == XTileStateSelected)
        return;
    
    Super::select();
    if(tile)
    {
        if(isSpecial())
        {
            tile->setSpriteFrame("SNSelec.png");
        }
        else
        {
            tile->setSpriteFrame("NumberSelec.png");
        }
    }
}

void XTileNumber::deselect()
{
    if(tileState == XTileStateDeselected)
        return;
    
    Super::deselect();
    if(tile)
    {
        if(isSpecial())
        {
            tile->setSpriteFrame("SpecialNumber.png");
        }
        else
        {
            tile->setSpriteFrame("Number.png");
        }
    }
}

void XTileNumber::indicateAsWrong()
{
    if(tileState == XTileStateWrong)
        return;

    Super::indicateAsWrong();
    if(tile)
    {
        if(isSpecial())
        {
            tile->setSpriteFrame("SNWrong.png");
        }
        else
        {
            tile->setSpriteFrame("NumberWrong.png");
        }
    }
}

XExpressionNumber* XTileNumber::generateExpressionItem() const
{
    XExpressionNumber* result = new XExpressionNumber(getNumber());
    result->setID(getName());
    result->setSpecial(isSpecial());
    return result;
}

void XTileNumber::setNumber(xuint number)
{
    this->number = number;
    if(title)
    {
        char buff[8];
        snprintf(buff, 8, "%u", number);
        static_cast<Label*>(title)->setString(buff);
    }
}

void XTileNumber::setSpecial(bool special)
{
    //TODO: Check this logic
    this->special = special;
    deselect();
}
