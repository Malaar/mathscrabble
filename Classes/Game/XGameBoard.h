//
//  XGameBoard.h
//  MathScrabble
//
//  Created by Malaar on 11/16/17.
//

#ifndef XGameBoard_h
#define XGameBoard_h

#include "cocos2d.h"
#include "XTileNumber.h"
#include "XTileOperator.h"
#include "XExpression.h"

class XGameBoard : public cocos2d::Sprite
{
public:
    CREATE_FUNC(XGameBoard);
    XGameBoard();
    virtual bool init();

    void reset();
    void clearBoard();
    void fillBoard();
    void selectTile(const cocos2d::Vec2& position);
    void deselectAllTiles();
    void clearSelection();
    bool isTileNeiborhoods(XTile* tile1, XTile* tile2) const;
    void clearUsedExpressions();
    bool isExpressionAlreadyUsed(const XExpression* expression) const;
    inline const XExpression* getCurrentExpression() const;
    bool useExpression(const XExpression* expression);
//    void unfreeze();
    void indicateAsWrong();
    inline const XTileVector& getSelectedTiles() const;

protected:
    void configureGUI();
    
    static const int stepsCount = 5;
    XTile* tiles[stepsCount][stepsCount];
    XExpression currentEpression;
    XTile* currentTile;
    XTileVector selectedTiles;
//    XLinesMaster linesMaster;
    XExpressionVector usedExpressions;
};

inline const XExpression* XGameBoard::getCurrentExpression() const
{
    return &currentEpression;
}

inline const XTileVector& XGameBoard::getSelectedTiles() const
{
    return selectedTiles;
}

#endif /* XGameBoard_h */
