//
//  XGameScene.cpp
//  MathScrabble
//
//  Created by Malaar on 11/15/17.
//

#include "XGameScene.h"
#include "ui/CocosGUI.h"
#include "2d/CCProgressTimer.h"
#include "XHomeScene.h"
#include "XFinalScene.h"

USING_NS_CC;

#define Super Scene
#define kShowReceivedScoreAction    1001
#define kGameDuration               120

XGameScene::XGameScene()
{
    scoreLabel = nullptr;
    receivedScoreLabel = nullptr;
    timeProgressNode = nullptr;
    gameBoard = nullptr;
    gameOverFlag = true;
    gameFreeze = false;
    totalScore = 0;
//    plaingTime = 60000;
//    currentPlaingTime = 0;
//    freezeTime = 20000;
//    currentFreezeTime = 0;
}

bool XGameScene::init()
{
    if(!Super::init())
    {
        return false;
    }
    
    loadAssets();
    configureGUI();
    
    auto touchListener = EventListenerTouchOneByOne::create();
    touchListener->setSwallowTouches(true);
    touchListener->onTouchBegan = CC_CALLBACK_2(XGameScene::onTouchBeganEvent, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(XGameScene::onTouchEndedEvent, this);
    touchListener->onTouchMoved = CC_CALLBACK_2(XGameScene::onTouchMovedEvent, this);
    touchListener->onTouchCancelled = CC_CALLBACK_2(XGameScene::onTouchEndedEvent, this);
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);
    
    return true;
}

void XGameScene::onEnter()
{
    Super::onEnter();
    
    if(gameBoard)
    {
        gameBoard->reset();
    }
}

void XGameScene::onEnterTransitionDidFinish()
{
    Super::onEnterTransitionDidFinish();
    
    startGame();
}

void XGameScene::loadAssets()
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Atlases/Game.plist");
}

void XGameScene::configureGUI()
{
    //
    Size size = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Vec2 center = Vec2(origin.x + size.width / 2, origin.y + size.height / 2);
    Vec2 leftTop = Vec2(origin.x, origin.y + size.height);
    Vec2 rightTop = Vec2(origin.x + size.width, origin.y + size.height);

    // Top Bar
    //
    auto bgNode = Sprite::createWithSpriteFrameName("Background.png");
    this->addChild(bgNode);
    bgNode->setPosition(center);
    
    //
    auto topBarNode = Sprite::createWithSpriteFrameName("ScoreBar.png");
    this->addChild(topBarNode);
    topBarNode->setPosition(center.x, leftTop.y - topBarNode->getContentSize().height * 0.5);
    
    //
    auto pauseButton = ui::Button::create("PauseButton-N.png", "PauseButton-H.png", "", ui::Widget::TextureResType::PLIST);
    topBarNode->addChild(pauseButton);
    pauseButton->setPositionNormalized(Vec2(0.155, 0.5));
    pauseButton->addTouchEventListener([](Ref* sender, ui::Widget::TouchEventType type) {
        if(type == ui::Widget::TouchEventType::ENDED)
        {
            Director::getInstance()->replaceScene(TransitionCrossFade::create(0.4, XHomeScene::create()));
            CCLOG("Pause pressed!");
        }
    });
    
    //
    auto freezeButton = ui::Button::create("FreezeButton-N.png", "FreezeButton-H.png", "", ui::Widget::TextureResType::PLIST);
    topBarNode->addChild(freezeButton);
    freezeButton->setPositionNormalized(Vec2(0.845, 0.5));
    freezeButton->addTouchEventListener([](Ref* sender, ui::Widget::TouchEventType type) {
        if(type == ui::Widget::TouchEventType::ENDED)
        {
            CCLOG("freeze pressed!");
        }
    });

    //
    scoreLabel = Label::createWithBMFont("fonts/Cooper34.fnt", "0");
    scoreLabel->setAnchorPoint(Vec2(1, 0.5));
    scoreLabel->setAdditionalKerning(35);
    topBarNode->addChild(scoreLabel);
    scoreLabel->setPositionNormalized(Vec2(0.78, 0.52));
    
    // Bottom Bar
    //
    auto bottomBarNode = Sprite::createWithSpriteFrameName("TimeBarBG.png");
    this->addChild(bottomBarNode);
    bottomBarNode->setPosition(center.x, origin.y + bottomBarNode->getContentSize().height * 0.5);

    //
    auto timebarShadowNode = Sprite::createWithSpriteFrameName("TimeBarShadow.png");
    bottomBarNode->addChild(timebarShadowNode);
    timebarShadowNode->setPosition(bottomBarNode->getContentSize().width * 0.5, timebarShadowNode->getContentSize().height * 0.5);
    
    //
    auto progressSprite = Sprite::createWithSpriteFrameName("TimeBarGreen.png");
    timeProgressNode = ProgressTimer::create(progressSprite);
    bottomBarNode->addChild(timeProgressNode);
    timeProgressNode->setType(ProgressTimer::Type::BAR);
    timeProgressNode->setPercentage(100);
    timeProgressNode->setMidpoint(Vec2(0, 0));
    timeProgressNode->setBarChangeRate(Vec2(1, 0));
    timeProgressNode->setPosition(bottomBarNode->getContentSize().width * 0.5, progressSprite->getContentSize().height * 0.5);

    // Game Board
    //
    gameBoard = XGameBoard::create();
    this->addChild(gameBoard);
    gameBoard->setPosition(center);
    if(getContentSize().width > size.width)
    {
        gameBoard->setScale(size.width / getContentSize().width);
    }
    receivedScoreLabel = Label::createWithBMFont("fonts/Cooper34.fnt", "0");
    this->addChild(receivedScoreLabel);
    receivedScoreLabel->setLocalZOrder(100);
    receivedScoreLabel->setVisible(false);
}

void XGameScene::startGame()
{
    gameOverFlag = false;
    gameFreeze = false;
//    currentPlaingTime = 0;
//    currentFreezeTime = 0;
//    touchDown = false;
//    playTimeSound = false;
//    freezeButton->SetControlState(XControl::XStateNormal);
    setTotalScore(0);
    receivedScoreLabel->stopAllActionsByTag(kShowReceivedScoreAction);
    receivedScoreLabel->setVisible(false);
    timeProgressNode->setPercentage(100);
    timeProgressNode->runAction(Sequence::create(ProgressTo::create(kGameDuration, 0), CallFuncN::create([&](Node* sender) {
        gameOver();
    }), nullptr));
//    Start();
//    XSoundManager::GetInstance()->StopBackgroundMusic();
//    if(gameBoard)
//    {
//        gameBoard->reset();
//    }
}

void XGameScene::pauseGame()
{
//    touchDown = false;
    if(gameBoard)
    {
        gameBoard->deselectAllTiles();
    }
//    Suspend();

//    XSoundManager::GetInstance()->StopBackgroundMusic();
//    playTimeSound = false;
}

void XGameScene::resumeGame()
{
//    if(!gameOverFlag)
//        Resume();
}

void XGameScene::gameOver()
{
    if(gameOverFlag)
        return;
    
    gameOverFlag = true;
    if(gameBoard)
    {
        gameBoard->deselectAllTiles();
    }
    
    scheduleOnce([&](float){
        Director::getInstance()->replaceScene(TransitionCrossFade::create(0.4, XFinalScene::create(totalScore)));
    }, 0.6, "transitionToFinalScreen");
    
//    Stop();
//    freezeButton->SetControlState(XControl::XStateDisabled);
//    XGame::GetInstance()->CompletePlaySession();

//    XSoundManager::GetInstance()->PlayBackgroundMusic(kXMainSoundTheme);

//    if(XGame::GetInstance()->GetCurrentMatch())
//    {
//        CompleteMatch();
//    }
//    else if(XGame::GetInstance()->GetCurrentTurnBasedMatch())
//    {
//        CompleteTurnBasedMatch();
//    }
//    else
//    {
//        // myScore, opponentScore, earnedCoins, isTurnBasedMatchCompleted, turnNumber
//        CallScriptGameDidOver(XGameResults::GetInstance()->GetCurrentScore(), "0", 0, true, false, 0);
//    }
}

void XGameScene::leaveGame(bool animateTransition)
{
    gameOverFlag = true;
    if(gameBoard)
    {
        gameBoard->deselectAllTiles();
    }
//    Stop();
//    freezeButton->SetControlState(XControl::XStateDisabled);
//    XSoundManager::GetInstance()->StopBackgroundMusic();
//    XSoundManager::GetInstance()->PlayBackgroundMusic(kXMainSoundTheme);

//    if(XGame::GetInstance()->GetCurrentMatch())
//    {
//        XGame::GetInstance()->GetCurrentMatch()->LeaveMatch();
//        XGame::GetInstance()->SetCurrentMatch(nullptr);
//    }
//    else if(XGame::GetInstance()->GetCurrentTurnBasedMatch())
//    {
//        XGame::GetInstance()->SetCurrentTurnBasedMatch(nullptr);
//    }

//    if(XLuaFunctionVoid::HasFunction(GetScene()->GetScript(), "PlayerDidLeaveGame"))
//    {
//        XLuaFunctionVoid function(GetScene()->GetScript(), "PlayerDidLeaveGame");
//        function();
//    }
}

void XGameScene::freeze()
{
    if(!gameFreeze)
    {
        gameFreeze = true;
//        currentFreezeTime = 0;
//        XSoundManager::GetInstance()->PlaySound(kXFreezeUsed);
    }
}

void XGameScene::unfreeze()
{
    if(gameFreeze)
    {
        gameFreeze = false;
//        currentFreezeTime = 0;
//        freezeButton->SetControlState(XControl::XStateNormal);
    }
}

bool XGameScene::onTouchBeganEvent(Touch* touch, Event* event)
{
    if(gameOverFlag)// || IsSuspended())
        return false;
    
    Vec2 position = touch->getLocation();
    if(gameBoard->getBoundingBox().containsPoint(position))
    {
        gameBoard->selectTile(position);
        return true;
    }

    return false;
}

bool XGameScene::onTouchEndedEvent(Touch* touch, Event* event)
{
    if(gameOverFlag)
        return false;
    
    if(gameBoard->useExpression(gameBoard->getCurrentExpression()))
    {
        XExpressionNumber number = gameBoard->getCurrentExpression()->evaluate();
        int numberValue = number.getNumber();
        
        if(numberValue != 0)
        {
            if(numberValue % 10 == 0)
            {
                if(number.isSpecial())
                    numberValue *= 2;
                CCLOG("evaluate: %i", numberValue);
                
                showReceivedScore(numberValue);
                gameBoard->deselectAllTiles();
                setTotalScore(totalScore + numberValue);
//                SetScore(XGameResults::GetInstance()->GetCurrentScore() + numberValue);
//                XSoundManager::GetInstance()->PlaySound(kXExpressionSuccess);
            }
            else
            {
                gameBoard->indicateAsWrong();
            }
        }
        else
        {
            gameBoard->deselectAllTiles();
        }
    }
    else
    {
        gameBoard->indicateAsWrong();
        CCLOG("XExpression already used");
    }

    return true;
}

bool XGameScene::onTouchMovedEvent(Touch* touch, Event* event)
{
    if(gameOverFlag)
        return false;
    
    bool result = false;
    
    Vec2 position = touch->getLocation();
    if(gameBoard->getBoundingBox().containsPoint(position))
    {
        gameBoard->selectTile(position);
        result = true;
    }
    else
    {
        gameBoard->deselectAllTiles();
        result = false;
    }

    return result;
}

void XGameScene::setTotalScore(int score)
{
    if(score < 0)
        score = 0;
    totalScore = score;
    if(scoreLabel)
    {
        scoreLabel->setString(std::to_string(score));
    }
}

void XGameScene::showReceivedScore(int receivedScore)
{
    Vec2 position;
    for(auto tile: gameBoard->getSelectedTiles())
    {
        position = (position + tile->getPosition());
    }
    position = position / gameBoard->getSelectedTiles().size();
    position = gameBoard->convertToWorldSpace(position);
    position = this->convertToNodeSpace(position);
    
    receivedScoreLabel->setPosition(position);
    receivedScoreLabel->setString(std::to_string(receivedScore));
    receivedScoreLabel->setVisible(true);
    receivedScoreLabel->setOpacity(51);
    receivedScoreLabel->setScale(0.5);
    auto action1 = Spawn::create(ScaleTo::create(0.2, 1), FadeTo::create(0.2, 255), nullptr);
    auto action2 = Spawn::create(ScaleTo::create(0.2, 2), FadeTo::create(0.2, 0), nullptr);
    auto action3 = CallFuncN::create([](Node* sender) {
        sender->setVisible(false);
    });
    auto action = Sequence::create(action1, action2, action3, nullptr);
    action->setTag(kShowReceivedScoreAction);
    receivedScoreLabel->stopAllActionsByTag(kShowReceivedScoreAction);
    receivedScoreLabel->runAction(action);
}
