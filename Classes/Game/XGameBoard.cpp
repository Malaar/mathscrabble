//
//  XGameBoard.cpp
//  MathScrabble
//
//  Created by Malaar on 11/16/17.
//

#include "XGameBoard.h"
#include "base/ccRandom.h"

USING_NS_CC;

#define Super Sprite

XGameBoard::XGameBoard()
{
    currentTile = nullptr;
    clearBoard();
}

bool XGameBoard::init()
{
    if(!Super::init())
    {
        return false;
    }
    configureGUI();
    currentEpression.setMaxOperatorsCount(2);
    currentEpression.setMaxGlidesCount(1);

    return true;
}

void XGameBoard::configureGUI()
{
    setSpriteFrame("PlayBG.png");
}

void XGameBoard::clearBoard()
{
    for(int i = 0; i < stepsCount; ++i)
    for(int j = 0; j < stepsCount; ++j)
    {
        tiles[i][j] = nullptr;
    }
    selectedTiles.clear();
    this->removeAllChildren();
}

void XGameBoard::reset()
{
    deselectAllTiles();
    clearUsedExpressions();
    fillBoard();
}

void XGameBoard::fillBoard()
{
    clearBoard();
    
    XTile* tile;
    
    // initial generation
    XTileVector initialTiles;
    xuint count = 16;
    for(int i = 0; i < count; ++i)
    {
        tile = XTileNumber::create();//tileNumberTemplate->Clone();
        ((XTileNumber*)tile)->setNumber(random(1, 9));//XRandom::GetRand(1,10));
        initialTiles.push_back(tile);
    }
    // only one special number per game
    int index = random(0, (int)initialTiles.size() - 1);//XRandom::GetRand(0, (int)initialTiles.size());
    ((XTileNumber*)initialTiles[index])->setSpecial(true);
    
    count = stepsCount * stepsCount - count;
    for(int i = 0; i < count; ++i)
    {
        tile = XTileOperator::create();//tileOperatorTemplate->Clone();
        XTileOperator::XTileOperatorType type = (XTileOperator::XTileOperatorType)random(0, 2);//XRandom::GetRand(0,3);
        ((XTileOperator*)tile)->setOperatorType(type);
        initialTiles.push_back(tile);
    }
    
    // first shuffle
    random_shuffle(initialTiles.begin(), initialTiles.end());
    
    // fill matrix
    Vec2 tileStartPosition(tile->getContentSize().width * 0.5, getContentSize().height - tile->getContentSize().height * 0.5);
    Vec2 tileOffset(tile->getContentSize().width * 0.9, tile->getContentSize().height * 0.9);
    Vec2 position;
    bool forceCreateNumber = false;
    bool forceCreateOperator = false;
    index = 0;
    for(int i = 0; i < stepsCount; ++i)
    for(int j = 0; j < stepsCount; ++j)
    {
        forceCreateNumber = false;
        forceCreateOperator = false;
        
        if(j > 0)
        {
            tile = tiles[i][j-1];
            if(tile)
            {
                //if(tile->GetObjectClassName() == XTileOperator::GetClassName())
                if(dynamic_cast<XTileOperator*>(tile))
                {
                    forceCreateNumber = true;
                }
                else if(j > 1)
                {
                    tile = tiles[i][j-2];
//                    if(tile && tile->GetObjectClassName() == XTileNumber::GetClassName())
                    if(tile && dynamic_cast<XTileNumber*>(tile))
                        forceCreateOperator = true;
                }
            }
        }
        if(i > 0)
        {
            tile = tiles[i-1][j];
            if(tile)
            {
//                if(tile->GetObjectClassName() == XTileOperator::GetClassName())
                if(dynamic_cast<XTileOperator*>(tile))
                {
                    forceCreateNumber = true;
                }
                else if(i > 1 && !forceCreateNumber)
                {
                    tile = tiles[i-2][j];
//                    if(tile && tile->GetObjectClassName() == XTileNumber::GetClassName())
                    if(tile && dynamic_cast<XTileNumber*>(tile))
                        forceCreateOperator = true;
                }
            }
        }
        
//        forceCreateNumber = forceCreateOperator = false;
        tile = nullptr;
        if(forceCreateNumber || forceCreateOperator)
        {
//            std::string className = forceCreateOperator ? XTileOperator::GetClassName() : XTileNumber::GetClassName();
            for(int i = 0; i < initialTiles.size(); ++i)
            {
//                if(initialTiles[i]->GetObjectClassName() == className)
                if((forceCreateOperator && dynamic_cast<XTileOperator*>(initialTiles[i])) ||
                   (forceCreateNumber && dynamic_cast<XTileNumber*>(initialTiles[i])) )
                {
                    tile = initialTiles[i];
                    initialTiles.erase(initialTiles.begin() + i);
                    break;
                }
            }
        }
        else
        {
            if(initialTiles.size())
            {
                tile = initialTiles.back();
                initialTiles.pop_back();
            }
        }
        
        // generate additional number tile if not enough
        if(!tile)
        {
            tile = XTileNumber::create();//tileNumberTemplate->Clone();
            ((XTileNumber*)tile)->setNumber(random(1, 9));//XRandom::GetRand(1,10));
        }
        
        if(tile)
        {
            tiles[i][j] = tile;
            char nameBuff[128];
            snprintf(nameBuff, 128, "tile%02i%02i", i, j);
//            std::string name = //XID::GetByName(XString::Format("tile%02i%02i", i, j));
            tile->setName(nameBuff);
            tile->indexI = i;
            tile->indexJ = j;
            addChild(tile);
            position = Vec2(tileStartPosition.x + tileOffset.x * j, tileStartPosition.y - tileOffset.y * i);
            tile->setPosition(position);
        }
//        else
//        {
//            XAssert(false, "title is nullptr!");
//        }
    }
}

void XGameBoard::selectTile(const Vec2& position)
{
//    Vec2 position = convertToNodeSpace(worldPosition);
    //TODO: Update it
    if(false)//GetScene()->HitTest(position) == this)
    {
        deselectAllTiles();
    }
    else
    {
        XTile* tile;
        for(int i = 0; i < stepsCount; ++i)
        for(int j = 0; j < stepsCount; ++j)
        {
            tile = tiles[i][j];
            if(tile && tile->hitTestTile(position))
            {
                if(tile != currentTile)
                {
                    currentTile = tile;
                    XTile* lastSelectedTile = selectedTiles.size() ? selectedTiles.back() : nullptr;
                    bool isTileNeigborhoods = lastSelectedTile ? isTileNeiborhoods(lastSelectedTile, tile) : true;
                    if(isTileNeigborhoods)
                    {
                        XExpressionItem* item = tile->generateExpressionItem();
                        if(currentEpression.pushItem(item))
                        {
                            // update green lines
                            if(lastSelectedTile && currentTile)
                            {
//                                linesMaster.LinkTails(lastSelectedTile, currentTile);
                            }
                            
                            //
                            tile->select();
                            selectedTiles.push_back(tile);
                            
                            //
                            //TODO: Play sound 'expression selected'
//                            int soundIndex = XRandom::GetRand(1, 7);
//                            XString soundName = XString::Format("Sounds/move%02i.caf", soundIndex);
//                            XSoundManager::GetInstance()->PlaySound(soundName);
                        }
                        else
                        {
                            CC_SAFE_DELETE(item);
                        }
                    }
                }
                break;
            }
        }
    }
}

void XGameBoard::deselectAllTiles()
{
    for(int i = 0; i < stepsCount; ++i)
    for(int j = 0; j < stepsCount; ++j)
    {
        if(tiles[i][j])
            tiles[i][j]->deselect();
    }
    clearSelection();
}

void XGameBoard::clearSelection()
{
    currentEpression.clearItems();
    currentTile = nullptr;
    selectedTiles.clear();
//    linesMaster.UnlinkAllTiles();
}

bool XGameBoard::isTileNeiborhoods(XTile* tile1, XTile* tile2) const
{
    bool result = false;
    if(tile1 && tile2)
    {
        result = (std::abs(tile1->indexI - tile2->indexI) <= 1) && (std::abs(tile1->indexJ - tile2->indexJ) <= 1);
    }
    return result;
}

//void XGameBoard::SetScore(int score)
//{
//    if(score < 0)
//        score = 0;
//    XGameResults::GetInstance()->SetCurrentScore(score);
//    if(scoreLabel)
//    {
//        scoreLabel->SetText(XString::Format("%i", score));
//    }
//
//    if(XGame::GetInstance()->GetCurrentMatch())
//    {
//        XGame::GetInstance()->GetCurrentMatch()->SendScore(score);
//    }
//}

void XGameBoard::clearUsedExpressions()
{
    XExpressionVectorIt it = usedExpressions.begin();
    XExpressionVectorIt end = usedExpressions.end();
    for(; it != end; ++it)
    {
        CC_SAFE_DELETE(*it);
    }
    usedExpressions.clear();
}

bool XGameBoard::isExpressionAlreadyUsed(const XExpression* expression) const
{
    bool result = false;
    if(expression)
    {
        XExpressionVectorConstIt it = usedExpressions.begin();
        XExpressionVectorConstIt end = usedExpressions.end();
        for(; it != end; ++it)
        {
            if((*it)->isEqual(expression))
            {
                result = true;
                break;
            }
        }
    }
    return result;
}

bool XGameBoard::useExpression(const XExpression* expression)
{
    bool result = false;
    if(!isExpressionAlreadyUsed(expression))
    {
        usedExpressions.push_back(expression->clone());
        result = true;
    }
    return result;
}

void XGameBoard::indicateAsWrong()
{
    XTileVectorIt it = selectedTiles.begin();
    XTileVectorIt end = selectedTiles.end();
    for(; it != end; ++it)
    {
        (*it)->indicateAsWrong();
    }
    clearSelection();
    //TODO: Play sound kXExpressionFailure
//    XSoundManager::GetInstance()->PlaySound(kXExpressionFailure);
}
