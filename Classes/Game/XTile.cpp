/* 
 * File:   XTile.cpp
 * Author: malaar
 * 
 * Created on 23 Март 2015 г., 15:02
 */

#include "XTile.h"

USING_NS_CC;

#define Super Sprite
#define kSelectionAction 1000001
XTile::XTile()
{
    tile = nullptr;
    title = nullptr;
    indexI = 0;
    indexJ = 0;
    tileState = XTileStateUndefined;
}

bool XTile::init()
{
    if(!Super::init())
    {
        return false;
    }
    
    //
    setSpriteFrame("NumberShadow.png");
    
    //
    tile = Sprite::createWithSpriteFrameName("Number.png");
    addChild(tile);
    tile->setPositionNormalized(Vec2(0.5f, 0.5f));
    
    return true;
}

bool XTile::hitTestTile(const Vec2& worldPosition) const
{
    bool result = false;
    if(tile)
    {
        Vec2 position = tile->convertToNodeSpace(worldPosition);
        
        Rect bbox = tile->getBoundingBox();
        float delta = 0.2f * bbox.size.width;
//        bbox.origin.x += delta;
//        bbox.origin.y += delta;
        bbox.size.width -= 1 * delta;
        bbox.size.height -= 1 * delta;
        
        result = bbox.containsPoint(position);
    }
    return result;
}

void XTile::select()
{
    if(tileState == XTileStateSelected)
        return;
    
    if(tile)
    {
        tile->setPosition(tile->getPosition().x, tile->getPosition().y + tile->getContentSize().height * 0.2f);

        auto spawn1 = Spawn::create(ScaleTo::create(0.2f, 1.1f), FadeTo::create(0.2f, 204), nullptr);
        auto spawn2 = Spawn::create(ScaleTo::create(0.2f, 1.0f), FadeTo::create(0.2f, 255), nullptr);
        auto selectionAction = Sequence::create(spawn1, spawn2, nullptr);
        selectionAction->setTag(kSelectionAction);
        tile->runAction(selectionAction);
    }
    tileState = XTileStateSelected;
}

void XTile::deselect()
{
    if(tileState == XTileStateDeselected)
        return;

    if(tile)
    {
        tile->stopActionByTag(kSelectionAction);
        if(tileState == XTileStateSelected || tileState == XTileStateWrong)
        {
            tile->setPosition(tile->getPosition().x, tile->getPosition().y - tile->getContentSize().height * 0.2f);
        }
        tile->setScale(1.0f);
        tile->setOpacity(255);
    }
    tileState = XTileStateDeselected;
}

void XTile::indicateAsWrong()
{
    if(tileState == XTileStateWrong || tileState == XTileStateDeselected)
        return;

    scheduleOnce([&](float) {
        deselect();
    }, 0.5f, "wrongWait");
    
    tileState = XTileStateWrong;
}
