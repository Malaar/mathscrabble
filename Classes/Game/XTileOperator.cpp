/* 
 * File:   XTileOperator.cpp
 * Author: malaar
 * 
 * Created on 23 Март 2015 г., 15:03
 */

#include "XTileOperator.h"
#include "XExpressionOperatorAdd.h"
#include "XExpressionOperatorSub.h"
#include "XExpressionOperatorMult.h"
#include "XExpressionOperatorDiv.h"

USING_NS_CC;

#define Super XTile

static const std::string kXAdditionFrame        = "Addition.png";
static const std::string kXSubtractionFrame     = "Subtraction.png";
static const std::string kXMultiplicationFrame  = "Multiplication.png";
static const std::string kXDivisionFrame        = "Division.png";

XTileOperator::XTileOperator()
{
    operatorType = XTileOperatorTypeAdd;
}

bool XTileOperator::init()
{
    if(!Super::init())
    {
        return false;
    }
    
    //
    if(tile)
    {
        tile->setSpriteFrame("Symbol.png");

        title = Sprite::createWithSpriteFrameName(kXDivisionFrame);
        tile->addChild(title);
        title->setPositionNormalized(Vec2(0.5, 0.53));
    }

    return true;
}

void XTileOperator::select()
{
    if(tileState == XTileStateSelected)
        return;
    
    Super::select();
    if(tile)
    {
        tile->setSpriteFrame("SymbolSelect.png");
    }
}

void XTileOperator::deselect()
{
    if(tileState == XTileStateDeselected)
        return;
    
    Super::deselect();
    if(tile)
    {
        tile->setSpriteFrame("Symbol.png");
    }
}

void XTileOperator::indicateAsWrong()
{
    if(tileState == XTileStateWrong)
        return;

    Super::indicateAsWrong();
    if(tile)
    {
        tile->setSpriteFrame("SymbolWrong.png");
    }
}

XExpressionOperator* XTileOperator::generateExpressionItem() const
{
    XExpressionOperator* result = nullptr;
    
    switch(getOperatorType())
    {
        case XTileOperatorTypeAdd:
            result = new XExpressionOperatorAdd();
            break;
        case XTileOperatorTypeSub:
            result = new XExpressionOperatorSub();
            break;
        case XTileOperatorTypeMult:
            result = new XExpressionOperatorMult();
            break;
        case XTileOperatorTypeDiv:
            result = new XExpressionOperatorDiv();
            break;
        default:
            break;
    }
    if(result)
        result->setID(getName());
    
    return result;
}

void XTileOperator::setOperatorType(XTileOperatorType type)
{
    operatorType = type;
    if(title)
    {
        std::string frameId;
        switch(type)
        {
            case XTileOperatorTypeAdd:
                frameId = kXAdditionFrame;
                break;
            case XTileOperatorTypeSub:
                frameId = kXSubtractionFrame;
                break;
            case XTileOperatorTypeMult:
                frameId = kXMultiplicationFrame;
                break;
            case XTileOperatorTypeDiv:
                frameId = kXDivisionFrame;
                break;
            default:
                frameId = kXAdditionFrame;
        }
        static_cast<Sprite*>(title)->setSpriteFrame(frameId);
    }
}
