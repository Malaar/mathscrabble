/* 
 * File:   XLinesMaster.h
 * Author: malaar
 *
 * Created on 27 Март 2015 г., 18:10
 */

#ifndef XLINESMASTER_H
#define	XLINESMASTER_H

#include "XTile.h"
#include "XSpritePool.h"

class XLinesMaster
{
public:
    XLinesMaster();
    
    void SetLinesOwner(XSprite* owner);
    xbool LinkTails(XTile* tile1, XTile* tile2);
    void UnlinkAllTiles();

private:
    XSpritePool pool;
    XSpriteVector lines;
    XSprite* owner;
};

#endif	/* XLINESMASTER_H */

