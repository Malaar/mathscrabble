/* 
 * File:   XTile.h
 * Author: malaar
 *
 * Created on 23 Март 2015 г., 15:02
 */

#ifndef XTILE_H
#define	XTILE_H

#include "cocos2d.h"
#include "XExpressionItem.h"

class XTile : public cocos2d::Sprite
{
public:
    XTile();
    
    virtual bool init();

    virtual void select();
    virtual void deselect();
    virtual void indicateAsWrong();
    
    virtual XExpressionItem* generateExpressionItem() const = 0;

    bool hitTestTile(const cocos2d::Vec2& position) const;

    int indexI;
    int indexJ;
    
protected:
    cocos2d::Sprite* tile;
    cocos2d::Node* title;
    
    enum XTileState
    {
        XTileStateUndefined,
        XTileStateDeselected,
        XTileStateSelected,
        XTileStateWrong
    };
    XTileState tileState;
};

typedef std::vector<XTile*> XTileVector;
typedef XTileVector::iterator XTileVectorIt;
typedef XTileVector::const_iterator XTileVectorConstIt;

#endif	/* XTILE_H */

