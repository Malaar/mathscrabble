/* 
 * File:   XTileNumber.h
 * Author: malaar
 *
 * Created on 23 Март 2015 г., 15:03
 */

#ifndef XTILENUMBER_H
#define	XTILENUMBER_H

#include "XTile.h"
#include "XExpressionNumber.h"

class XTileNumber : public XTile
{
public:
    CREATE_FUNC(XTileNumber);
    XTileNumber();

    virtual bool init();

    virtual void select();
    virtual void deselect();
    virtual void indicateAsWrong();

    virtual XExpressionNumber* generateExpressionItem() const;

    void setNumber(xuint number);
    inline xuint getNumber() const;
    
    void setSpecial(bool special);
    inline bool isSpecial() const;
    
private:
    xuint number;
    bool special;
};

inline xuint XTileNumber::getNumber() const
{
    return number;
}

inline bool XTileNumber::isSpecial() const
{
    return special;
}

#endif	/* XTILENUMBER_H */
