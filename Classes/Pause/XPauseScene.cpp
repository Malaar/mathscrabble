//
//  XPauseScene.cpp
//  MathScrabble
//
//  Created by Malaar on 11/15/17.
//

#include "XPauseScene.h"

USING_NS_CC;

#define Super Scene

bool XPauseScene::init()
{
    if(!Super::init())
    {
        return false;
    }
    
    return true;
}
