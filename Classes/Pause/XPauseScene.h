//
//  XPauseScene.h
//  MathScrabble
//
//  Created by Malaar on 11/15/17.
//

#ifndef XPauseScene_h
#define XPauseScene_h

#include "cocos2d.h"

class XPauseScene : public cocos2d::Scene
{
public:
    CREATE_FUNC(XPauseScene);
    virtual bool init();
};

#endif /* XPauseScene_h */
