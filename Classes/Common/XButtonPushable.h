//
//  XButtonPushable.h
//  MathScrabble
//
//  Created by Malaar on 11/16/17.
//

#ifndef XButtonPushable_h
#define XButtonPushable_h

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class XButtonPushable : public cocos2d::ui::Button
{
public:
    static XButtonPushable* create();
    static XButtonPushable* create(const std::string& normalImage,
                                  const std::string& selectedImage = "",
                                  const std::string& disableImage = "",
                                  TextureResType texType = TextureResType::LOCAL);

    XButtonPushable();

    virtual void onPressStateChangedToNormal();
    virtual void onPressStateChangedToPressed();
    virtual void onPressStateChangedToDisabled();

    /**
     * Setup child node of button which will be moved when button pressed (has highlighted state).
     */
    void setPushableNode(Node* node, float pushDelta = 3.0f);
    
protected:
    Node* pushableNode;
    cocos2d::Vec2 pushableOriginalPosition;
    float pushDelta;
};

#endif /* XButtonPushable_h */
