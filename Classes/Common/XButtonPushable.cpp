//
//  XButtonPushable.cpp
//  MathScrabble
//
//  Created by Malaar on 11/16/17.
//

#include "XButtonPushable.h"

USING_NS_CC;
using namespace ui;

#define Super Button

XButtonPushable::XButtonPushable()
              : pushableNode(nullptr)
              , pushDelta(0.0f)
{
}

XButtonPushable* XButtonPushable::create()
{
    XButtonPushable* button = new (std::nothrow) XButtonPushable();
    if (button && button->init())
    {
        button->autorelease();
        return button;
    }
    CC_SAFE_DELETE(button);
    return nullptr;
}

XButtonPushable* XButtonPushable::create(const std::string& normalImage,
                                       const std::string& selectedImage,
                                       const std::string& disableImage,
                                       TextureResType texType)
{
    XButtonPushable *button = new (std::nothrow) XButtonPushable();
    if (button && button->init(normalImage,selectedImage,disableImage,texType))
    {
        button->autorelease();
        return button;
    }
    CC_SAFE_DELETE(button);
    return nullptr;
}

void XButtonPushable::onPressStateChangedToNormal()
{
    Super::onPressStateChangedToNormal();
    if(pushableNode)
    {
        pushableNode->setPosition(pushableOriginalPosition);
    }
}

void XButtonPushable::onPressStateChangedToPressed()
{
    Super::onPressStateChangedToPressed();
    if(pushableNode)
    {
        pushableNode->setPosition(pushableOriginalPosition);
        pushableNode->setPosition(pushableOriginalPosition.x, pushableOriginalPosition.y - 3);
    }
}

void XButtonPushable::onPressStateChangedToDisabled()
{
    Super::onPressStateChangedToDisabled();
    if(pushableNode)
    {
        pushableNode->setPosition(pushableOriginalPosition);
    }
}

void XButtonPushable::setPushableNode(Node* node, float pushDelta)
{
    if(node && node->getParent() == this)
    {
        pushableNode = node;
        pushableOriginalPosition = pushableNode->getPosition();
        this->pushDelta = pushDelta;
    }
    else
    {
        pushableNode = nullptr;
        pushableOriginalPosition = Vec2::ZERO;
        pushDelta = 0.0f;
    }
}
