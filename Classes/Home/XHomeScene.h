//
//  XHomeScene.h
//  MathScrabble
//
//  Created by Malaar on 11/15/17.
//

#ifndef XHomeScene_h
#define XHomeScene_h

#include "cocos2d.h"

class XHomeScene : public cocos2d::Scene
{
public:
    CREATE_FUNC(XHomeScene);
    virtual bool init();
    
protected:
    void loadAssets();
    void configureGUI();
};

#endif /* XHomeScene_h */
