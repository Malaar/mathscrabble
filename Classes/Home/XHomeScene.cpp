//
//  XHomeScene.cpp
//  MathScrabble
//
//  Created by Malaar on 11/15/17.
//

#include "XHomeScene.h"
#include "ui/CocosGUI.h"
#include "XTutorialScene.h"
#include "XGameScene.h"
#include "XButtonPushable.h"

USING_NS_CC;

#define Super Scene

bool XHomeScene::init()
{
    if(!Super::init())
    {
        return false;
    }
    
    loadAssets();
    configureGUI();
    
    return true;
}

void XHomeScene::loadAssets()
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Atlases/Home.plist");
}

void XHomeScene::configureGUI()
{
    //
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Size size = Director::getInstance()->getVisibleSize();
    Vec2 center = Vec2(origin.x + size.width / 2, origin.y + size.height / 2);
    
    //
    auto bgNode = Sprite::createWithSpriteFrameName("Background.png");
    this->addChild(bgNode);
    bgNode->setPosition(center);
    
    //
    auto titleNode = Sprite::createWithSpriteFrameName("Title.png");
    this->addChild(titleNode);
    titleNode->setPosition(center.x, origin.y + size.height - titleNode->getContentSize().height);
    if(titleNode->getContentSize().width > size.width)
    {
        titleNode->setScale(size.width / titleNode->getContentSize().width);
    }

    //
    auto playButton = XButtonPushable::create("ButtonMultiplay-N.png", "ButtonMultiplay-H.png", "", ui::Widget::TextureResType::PLIST);
    this->addChild(playButton);
    playButton->setPosition(Vec2(center.x, center.y - 50));
    auto label = Label::createWithBMFont("fonts/Cooper30.fnt", "Play");
    label->setAnchorPoint(Point(0.5, 0.5));
    playButton->setTitleLabel(label);
    playButton->setPushableNode(label);
    playButton->addTouchEventListener([](Ref* sender, ui::Widget::TouchEventType type) {
        if(type == ui::Widget::TouchEventType::ENDED)
        {
            Director::getInstance()->replaceScene(TransitionCrossFade::create(0.4, XGameScene::create()));
        }
    });

    //
    auto tutorialButton = XButtonPushable::create("ButtonLittle-N.png", "ButtonLittle-H.png", "", ui::Widget::TextureResType::PLIST);
    this->addChild(tutorialButton);
    tutorialButton->setPosition(Vec2(center.x - tutorialButton->getContentSize().width * 0.7,
                                     origin.y + tutorialButton->getContentSize().height));
    auto imgNode = Sprite::createWithSpriteFrameName("HowToPlayImg.png");
    tutorialButton->addChild(imgNode);
    imgNode->setPosition(0.5 * imgNode->getParent()->getContentSize().width,
                         0.54 * imgNode->getParent()->getContentSize().height);
    tutorialButton->setPushableNode(imgNode);
    tutorialButton->addTouchEventListener([](Ref* sender, ui::Widget::TouchEventType type) {
//        Director::getInstance()->replaceScene(TransitionCrossFade::create(0.4, XTutorialScene::create()));
        Director::getInstance()->pushScene(TransitionCrossFade::create(0.4, XTutorialScene::create()));
    });

    //
    auto shareButton = XButtonPushable::create("ButtonLittle-N.png", "ButtonLittle-H.png", "", ui::Widget::TextureResType::PLIST);
    this->addChild(shareButton);
    shareButton->setPosition(Vec2(center.x + shareButton->getContentSize().width * 0.7,
                                  origin.y + shareButton->getContentSize().height));
    imgNode = Sprite::createWithSpriteFrameName("ShareImg.png");
    shareButton->addChild(imgNode);
    imgNode->setPosition(0.5 * imgNode->getParent()->getContentSize().width,
                         0.54 * imgNode->getParent()->getContentSize().height);
    shareButton->setPushableNode(imgNode);
}
