//
//  XTutorialScene.h
//  MathScrabble
//
//  Created by Malaar on 11/15/17.
//

#ifndef XTutorialScene_h
#define XTutorialScene_h

#include "cocos2d.h"
#include "ui/UILayout.h"
#include "ui/UIPageViewIndicator.h"
#include "ui/UIButton.h"
#include "XButtonPushable.h"

class XTutorialScene : public cocos2d::Scene
{
public:
    static XTutorialScene* create(bool playAfterTutorial = false);
    XTutorialScene();
    virtual bool init(bool playAfterTutorial);

    void loadAssets();
    void configureGUI();
    
private:
    cocos2d::ui::Layout* createPage(const std::string& imageName, const std::string& titleText);

    bool playAfterTutorial;
    cocos2d::ui::PageViewIndicator* pageIndicator;
    XButtonPushable* nextButton;
    cocos2d::ui::Button* closeButton;
};

#endif /* XTutorialScene_h */
