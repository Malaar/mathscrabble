//
//  XTutorialScene.cpp
//  MathScrabble
//
//  Created by Malaar on 11/15/17.
//

#include "XTutorialScene.h"
#include "XGameScene.h"
#include "ui/UIPageView.h"

USING_NS_CC;

#define Super Scene

XTutorialScene::XTutorialScene()
{
    pageIndicator = nullptr;
    playAfterTutorial = false;
}

XTutorialScene* XTutorialScene::create(bool playAfterTutorial)
{
    auto scene = new (std::nothrow) XTutorialScene();
    if(scene && scene->init(playAfterTutorial))
    {
        scene->autorelease();
    }
    else
    {
        CC_SAFE_DELETE(scene);
    }
    return scene;
}

bool XTutorialScene::init(bool playAfterTutorial)
{
    if(!Super::init())
    {
        return false;
    }
    
    this->playAfterTutorial = playAfterTutorial;
    loadAssets();
    configureGUI();
    
    return true;
}

void XTutorialScene::loadAssets()
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Atlases/Tutorial.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Atlases/Final.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Atlases/Home.plist");
}

void XTutorialScene::configureGUI()
{
    //
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Size size = Director::getInstance()->getVisibleSize();
    Vec2 center = Vec2(origin.x + size.width / 2, origin.y + size.height / 2);

    //
    auto bgNode = Sprite::createWithSpriteFrameName("Background.png");
    this->addChild(bgNode);
    bgNode->setPosition(center);

    //
    ui::PageView* pageView = ui::PageView::create();
    this->addChild(pageView);
    pageView->setPosition(Vec2(origin.x, origin.y + 0.1 * size.height));
    pageView->setContentSize(Size(size.width, size.height - 0.1 * size.height));
    pageView->setDirection(cocos2d::ui::ScrollView::Direction::HORIZONTAL);
    pageView->setBounceEnabled(true);
    pageView->setTouchEnabled(true);
    
    //
    ui::Layout* page1 = createPage("TutorialImg1.png", "Match symbols and numbers in the equation");
    pageView->addPage(page1);

    ui::Layout* page2 = createPage("TutorialImg2.png", "Get points when the result is 10, 20... or more");
    pageView->addPage(page2);

    ui::Layout* page3 = createPage("TutorialImg3.png", "Connect consequtive numbers into the two-digit numbers");
    pageView->addPage(page3);

    ui::Layout* page4 = createPage("TutorialImg4.png", "Use a special number to double your result");
    pageView->addPage(page4);
    
    //
    pageIndicator = ui::PageViewIndicator::create();
    this->addChild(pageIndicator);
    pageIndicator->setIndexNodesTexture("SelectedPoint.png", ui::Widget::TextureResType::PLIST);
    pageIndicator->setIndexNodesColor(Color3B(255, 255, 255));
    pageIndicator->setSelectedIndexColor(Color3B(255, 255, 255));
    pageIndicator->setIndexNodesOpacity(128);
    pageIndicator->setSelectedIndexOpacity(255);
    pageIndicator->setPosition(center.x, origin.y + 0.05 * size.height);
    pageIndicator->reset(pageView->getItems().size());
    pageIndicator->indicate(0);
    
    //
    nextButton = XButtonPushable::create("ButtonAgain-N.png", "ButtonAgain-H.png", "", ui::Widget::TextureResType::PLIST);
    this->addChild(nextButton);
    nextButton->setPosition(Vec2(center.x, origin.y + 0.1 * size.height));
    auto imgNode = Sprite::createWithSpriteFrameName("PlayImg.png");
    nextButton->addProtectedChild(imgNode);
    imgNode->setPosition(0.5 * imgNode->getParent()->getContentSize().width,
                         0.54 * imgNode->getParent()->getContentSize().height);
    nextButton->setPushableNode(imgNode);
    nextButton->setVisible(false);
    nextButton->addTouchEventListener([](Ref* sender, ui::Widget::TouchEventType type) {
        if(type == ui::Widget::TouchEventType::ENDED)
        {
            Director::getInstance()->replaceScene(TransitionCrossFade::create(0.4, XGameScene::create()));
        }
    });
    
    //
    if(!playAfterTutorial)
    {
        closeButton = ui::Button::create("CloseButton.png", "", "", ui::Widget::TextureResType::PLIST);
        this->addChild(closeButton);
        closeButton->setPosition(Vec2(origin.x + size.width * 0.1, origin.y + size.height * 0.95));
//        closeButton->setVisible(false);
        closeButton->addTouchEventListener([](Ref* sender, ui::Widget::TouchEventType type) {
            if(type == ui::Widget::TouchEventType::ENDED)
            {
                Director::getInstance()->popScene();
            }
        });
    }
    
    pageView->addEventListener([&](Ref* sender, ui::PageView::EventType type) {
        if(type == ui::PageView::EventType::TURNING)
        {
            auto pageView = static_cast<ui::PageView*>(sender);
            ssize_t index = pageView->getCurrentPageIndex();
            pageIndicator->indicate(index);
            if(playAfterTutorial)
            {
                if(index == pageView->getItems().size() - 1)
                {
                    pageIndicator->runAction(FadeOut::create(0.4));
                    nextButton->setVisible(true);
                    nextButton->setOpacity(0);
                    nextButton->runAction(FadeIn::create(0.4));
                }
                else
                {
                    pageIndicator->runAction(FadeIn::create(0.4));
                    nextButton->runAction(FadeOut::create(0.4));
                }

            }
        }
    });
    
}

ui::Layout* XTutorialScene::createPage(const std::string& imageName, const std::string& titleText)
{
    ui::Layout* page = ui::Layout::create();

    auto pageBgNode = Sprite::createWithSpriteFrameName("TutorialBG.png");
    page->addChild(pageBgNode);
    pageBgNode->setPositionNormalized(Vec2(0.5, 0.5));
    
    auto tutImgNode = Sprite::createWithSpriteFrameName(imageName);
    pageBgNode->addChild(tutImgNode);
    tutImgNode->setPositionNormalized(Vec2(0.5, 0.4));
    
    auto tutLabel = Label::createWithBMFont("fonts/Cooper18.fnt", titleText);
    pageBgNode->addChild(tutLabel);
    tutLabel->setPositionNormalized(Vec2(0.5, 0.85));
    tutLabel->setDimensions(pageBgNode->getContentSize().width * 0.8, 0);
    tutLabel->setAlignment(cocos2d::TextHAlignment::CENTER);
    
    Size size = Director::getInstance()->getVisibleSize();
    if(pageBgNode->getContentSize().width > size.width)
    {
        pageBgNode->setScale(size.width / pageBgNode->getContentSize().width);
    }
    
    return page;
}
