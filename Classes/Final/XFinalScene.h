//
//  XFinalScene.h
//  MathScrabble
//
//  Created by Malaar on 11/15/17.
//

#ifndef XFinalScene_h
#define XFinalScene_h

#include "cocos2d.h"

class XFinalScene : public cocos2d::Scene
{
public:
    static XFinalScene* create(int currentScore = 0);
    virtual bool init(int currentScore);

protected:
    void loadAssets();
    void configureGUI();
    
private:
    cocos2d::Label* scoreLabel;
    cocos2d::Label* bestScoreLabel;
    int score;
};

#endif /* XFinalScene_h */
