//
//  XFinalScene.cpp
//  MathScrabble
//
//  Created by Malaar on 11/15/17.
//

#include "XFinalScene.h"
#include "XTutorialScene.h"
#include "XGameScene.h"
#include "XHomeScene.h"
#include "XButtonPushable.h"

USING_NS_CC;

#define Super Scene

XFinalScene* XFinalScene::create(int currentScore)
{
    auto scene = new (std::nothrow) XFinalScene();
    if(scene && scene->init(currentScore))
    {
        scene->autorelease();
    }
    else
    {
        CC_SAFE_DELETE(scene);
    }
    return scene;
}

bool XFinalScene::init(int currentScore)
{
    if(!Super::init())
    {
        return false;
    }
    
    score = currentScore;
    loadAssets();
    configureGUI();
    
    if(score > UserDefault::getInstance()->getIntegerForKey(kBestScoreKey))
    {
        UserDefault::getInstance()->setIntegerForKey(kBestScoreKey, score);
        UserDefault::getInstance()->flush();
    }
    if(false == UserDefault::getInstance()->getBoolForKey(kDisableShowTutorialAtStart, false))
    {
        UserDefault::getInstance()->setBoolForKey(kDisableShowTutorialAtStart, true);
        UserDefault::getInstance()->flush();
    }
    return true;
}

void XFinalScene::loadAssets()
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Atlases/Final.plist");
}

void XFinalScene::configureGUI()
{
    //
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Size size = Director::getInstance()->getVisibleSize();
    Vec2 center = Vec2(origin.x + size.width / 2, origin.y + size.height / 2);
    
    //
    auto bgNode = Sprite::createWithSpriteFrameName("Background.png");
    this->addChild(bgNode);
    bgNode->setPosition(center);
    
    //
    auto titleNode = Sprite::createWithSpriteFrameName("Congrat.png");
    this->addChild(titleNode);
    if(titleNode->getContentSize().width > size.width)
    {
        titleNode->setScale(size.width / titleNode->getContentSize().width);
    }
    titleNode->setPosition(center.x, origin.y + size.height - titleNode->getContentSize().height * titleNode->getScale() * 0.5);
    
    //
    auto finalBgNode = Sprite::createWithSpriteFrameName("FinalBG.png");
    this->addChild(finalBgNode);
    finalBgNode->setPosition(center.x, origin.y + size.height - titleNode->getBoundingBox().size.height - finalBgNode->getBoundingBox().size.height * 0.45);
    if(getContentSize().width > size.width)
    {
        finalBgNode->setScale(size.width / getContentSize().width);
    }
    
    //
    auto scoreTitleLabel = Label::createWithBMFont("fonts/Cooper24.fnt", "Score:");
    finalBgNode->addChild(scoreTitleLabel);
    scoreTitleLabel->setAnchorPoint(Vec2(0, 0.5));
    scoreTitleLabel->setPositionNormalized(Vec2(0.15, 0.6));

    //
    auto scoreLabel = Label::createWithBMFont("fonts/Cooper24.fnt", std::to_string(score));
    finalBgNode->addChild(scoreLabel);
    scoreLabel->setAnchorPoint(Vec2(1.0, 0.5));
    scoreLabel->setPositionNormalized(Vec2(0.85, 0.6));

    //
    auto bestScoreTitleLabel = Label::createWithBMFont("fonts/Cooper24.fnt", "Best:");
    finalBgNode->addChild(bestScoreTitleLabel);
    bestScoreTitleLabel->setAnchorPoint(Vec2(0, 0.5));
    bestScoreTitleLabel->setPositionNormalized(Vec2(0.15, 0.3));

    //
    auto bestScoreLabel = Label::createWithBMFont("fonts/Cooper24.fnt", "0");
    finalBgNode->addChild(bestScoreLabel);
    bestScoreLabel->setAnchorPoint(Vec2(1.0, 0.5));
    bestScoreLabel->setPositionNormalized(Vec2(0.85, 0.3));
    int bestScore = UserDefault::getInstance()->getIntegerForKey(kBestScoreKey);
    bestScoreLabel->setString(std::to_string(bestScore));
    
    //
    auto againButton = XButtonPushable::create("ButtonAgain-N.png", "ButtonAgain-H.png", "", ui::Widget::TextureResType::PLIST);
    this->addChild(againButton);
    againButton->setPosition(Vec2(center.x, origin.y + againButton->getContentSize().height * 1.8));
    auto label = Label::createWithBMFont("fonts/Cooper24.fnt", "Again");
    label->setAnchorPoint(Point(0.5, 0.55));
    againButton->setTitleLabel(label);
    againButton->setPushableNode(label);
    againButton->addTouchEventListener([](Ref* sender, ui::Widget::TouchEventType type) {
        if(type == ui::Widget::TouchEventType::ENDED)
        {
            Director::getInstance()->replaceScene(TransitionCrossFade::create(0.4, XGameScene::create()));
        }
    });
    
    //
    auto homeButton = XButtonPushable::create("ButtonLittle-N.png", "ButtonLittle-H.png", "", ui::Widget::TextureResType::PLIST);
    this->addChild(homeButton);
    homeButton->setPosition(Vec2(center.x - homeButton->getContentSize().width * 1.2,
                                 origin.y + homeButton->getContentSize().height * 0.7));
    auto imgNode = Sprite::createWithSpriteFrameName("HomeImg.png");
    homeButton->addChild(imgNode);
    imgNode->setPosition(0.5 * imgNode->getParent()->getContentSize().width,
                         0.54 * imgNode->getParent()->getContentSize().height);
    homeButton->setPushableNode(imgNode);
    homeButton->addTouchEventListener([](Ref* sender, ui::Widget::TouchEventType type) {
        if(type == ui::Widget::TouchEventType::ENDED)
        {
            Director::getInstance()->replaceScene(TransitionCrossFade::create(0.4, XHomeScene::create()));
        }
    });

    //
    auto tutorialButton = XButtonPushable::create("ButtonLittle-N.png", "ButtonLittle-H.png", "", ui::Widget::TextureResType::PLIST);
    this->addChild(tutorialButton);
    tutorialButton->setPosition(Vec2(center.x,
                                     origin.y + tutorialButton->getContentSize().height * 0.7));
    imgNode = Sprite::createWithSpriteFrameName("HowToPlayImg.png");
    tutorialButton->addChild(imgNode);
    imgNode->setPosition(0.5 * imgNode->getParent()->getContentSize().width,
                         0.54 * imgNode->getParent()->getContentSize().height);
    tutorialButton->setPushableNode(imgNode);
    tutorialButton->addTouchEventListener([](Ref* sender, ui::Widget::TouchEventType type) {
        if(type == ui::Widget::TouchEventType::ENDED)
        {
            Director::getInstance()->pushScene(TransitionCrossFade::create(0.4, XTutorialScene::create()));
        }
    });
    
    //
    auto shareButton = XButtonPushable::create("ButtonLittle-N.png", "ButtonLittle-H.png", "", ui::Widget::TextureResType::PLIST);
    this->addChild(shareButton);
    shareButton->setPosition(Vec2(center.x + shareButton->getContentSize().width * 1.2,
                                  origin.y + shareButton->getContentSize().height * 0.7));
    imgNode = Sprite::createWithSpriteFrameName("ShareImg.png");
    shareButton->addChild(imgNode);
    imgNode->setPosition(0.5 * imgNode->getParent()->getContentSize().width,
                         0.54 * imgNode->getParent()->getContentSize().height);
    shareButton->setPushableNode(imgNode);

}
